#include <iostream>

#include "RfamEntry.h"
#include "RfamDatabase.h"

#include <fstream>
#include <sstream>

std::vector<double> DoubleStrandedCoverage(const std::string& strandedness, std::size_t begin, std::size_t end) {
  std::size_t ds = 0;
  std::size_t maxSsRegion = 0;
  std::size_t ssRegion = 0;
  for (std::size_t i = begin; i < end; ++i) {
    if (strandedness[i] == '=') {
      ++ds;
      if (ssRegion > maxSsRegion) {
	maxSsRegion = ssRegion;
      }
      ssRegion = 0;
    }
    else {
      ++ssRegion;
    }
  }
  if (ssRegion > maxSsRegion) {
    maxSsRegion = ssRegion;
    ssRegion = 0;
  }
  std::vector<double> results;
  results.push_back(static_cast<double>(ds)/(end-begin));
  results.push_back(maxSsRegion);
  return results;
}

void DoubleStrandedCoverage(std::string readsFile, std::string strandednessFile) {
  std::vector<std::vector<std::string> > strands;
  {
    std::ifstream inputstr(strandednessFile.c_str());
    std::string line;
    std::vector<std::string> strand;
    int counter = 0;
    while(std::getline(inputstr, line)) {
      ++counter;
      if (counter == 1) {
	strand.push_back(line.substr(1));
      }
      else {
	strand.push_back(line);
      }
      if (counter == 3) {
	strands.push_back(strand);
	strand.clear();
	counter = 0;
      }
    }
  }
  typedef std::pair<std::pair<std::string, std::string>,std::pair<int, int> > read_t;
  std::vector<read_t> reads;
  {
    std::ifstream inputReads(readsFile.c_str());
    std::string line;
    while(std::getline(inputReads, line)) {
      std::stringstream read(line);
      std::string readName;
      std::string strandName;
      std::size_t begin;
      std::size_t end;
      read >> readName;
      read >> strandName;
      read >> begin;
      read >> end;
      reads.push_back(std::make_pair(std::make_pair(readName, strandName), std::make_pair(begin, end)));
    }
  }
  for (std::vector<read_t>::const_iterator it = reads.begin(); it != reads.end(); ++it) {
    std::string readName = it->first.first;
    std::string readStrand = it->first.second;
    int begin = it->second.first;
    int end = it->second.second;
    for (std::vector<std::vector<std::string> >::const_iterator st = strands.begin(); st != strands.end(); ++st) {
      std::string strandName = (*st)[0];
      std::string strandSequence = (*st)[1];
      std::string strandedness = (*st)[2];

      if (readStrand == strandName) {
	std::vector<double> results = DoubleStrandedCoverage(strandedness, begin, end);
	std::cout << readName << "\t" << strandName << "\t" << results[0] << "\t" << results[1] << std::endl;
      }
    }
  }
}

void DoubleStrandedness(std::string readsFile, std::string strandednessFile) {
  std::vector<std::vector<std::string> > strands;
  {
    std::ifstream inputstr(strandednessFile.c_str());
    std::string line;
    std::vector<std::string> strand;
    int counter = 0;
    while(std::getline(inputstr, line)) {
      ++counter;
      if (counter == 1) {
	strand.push_back(line.substr(1));
      }
      else {
	strand.push_back(line);
      }
      if (counter == 3) {
	strands.push_back(strand);
	strand.clear();
	counter = 0;
      }
    }
  }
  typedef std::pair<std::pair<std::string, std::string>,std::pair<int, int> > read_t;
  std::vector<read_t> reads;
  {
    std::ifstream inputReads(readsFile.c_str());
    std::string line;
    while(std::getline(inputReads, line)) {
      std::stringstream read(line);
      std::string readName;
      std::string strandName;
      std::size_t begin;
      std::size_t end;
      read >> readName;
      read >> strandName;
      read >> begin;
      read >> end;
      reads.push_back(std::make_pair(std::make_pair(readName, strandName), std::make_pair(begin, end)));
    }
  }
  for (std::vector<read_t>::const_iterator it = reads.begin(); it != reads.end(); ++it) {
    std::string readName = it->first.first;
    std::string readStrand = it->first.second;
    int begin = it->second.first;
    int end = it->second.second;
    for (std::vector<std::vector<std::string> >::const_iterator st = strands.begin(); st != strands.end(); ++st) {
      std::string strandName = (*st)[0];
      std::string strandSequence = (*st)[1];
      std::string strandedness = (*st)[2];
      if (readStrand == strandName) {
	std::string readStrandedness = strandedness.substr(begin, end - begin);
	std::cout << '>' << readName << std::endl;
	std::cout << readStrandedness << std::endl;
      }
    }
  }
}

int main(int argc, const char * argv[]) {
  if (argc == 1) {
    RfamDatabase database = RfamDatabase("../TestData/Rfam.seed");
    //database.Format();
    //database.Tabular();
    DoubleStrandedCoverage("../TestData/S4_Mapped.txt", "../TestData/SecondaryStructures.txt");
  }
}
