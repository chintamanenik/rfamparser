//
//  RfamParser.cpp
//  RfamParser
//
//  Created by Kundan Chintamaneni on 7/19/15.
//  Copyright (c) 2015 Kundan Chintamaneni. All rights reserved.
//

#include "RfamDatabase.h"

#include <iostream>
#include <fstream>
#include <sstream>

void RfamDatabase::Format() {
  for (std::vector<RfamEntry>::const_iterator it = database.begin(); it != database.end(); ++it) {
    std::vector<alignment> alignments = it->Alignments();
    for (std::vector<alignment>::const_iterator al = alignments.begin(); al != alignments.end(); ++al) {
      std::cout << ">" << al->first << std::endl;
      for (std::size_t index = 0; index < al->second.length(); ++index) {
        const char ch = al->second[index];
        if (ch != '-') {
            std::cout << ch;
        }
      }
      std::cout << std::endl;
      for (std::size_t index = 0; index < al->second.length(); ++index) {
        if (al->second[index] != '-') {
	  const char ch = it->ConservedSecondaryStructure()[index];
	  if (IsDoubleStranded(ch)) {
	    std::cout << '=';
	  }
	  else {
	    std::cout << '-';
	  }
        }
      }
      std::cout << std::endl;
     }
  }
}

void RfamDatabase::Tabular() {
for (std::vector<RfamEntry>::const_iterator it = database.begin(); it != database.end(); ++it) {
    std::vector<alignment> alignments = it->Alignments();
    for (std::vector<alignment>::const_iterator al = alignments.begin(); al != alignments.end(); ++al) {
      std::cout << al->first << "\t" << it->Name() << std::endl;
     }
  }
}

RfamDatabase::RfamDatabase(const std::string& filename) {
  std::vector<RfamEntry> entries;
  std::ifstream input(filename.c_str());
  std::string line;
  std::cerr << "Processing Database: " << filename.c_str() << std::endl;
  std::string name;
  std::string conservedSequence;
  std::string conservedSecondaryStructure;
  std::vector<alignment> alignments;
  while (std::getline(input, line)) {
    std::size_t length = line.length();
    if (length > 0) {
      std::stringstream iss(line);
      std::string token;
      iss >> token;
      if (token == "#=GF") {
        iss >> token;
        if (token == "ID") {
          iss >> name;
        }
      }
      else if (token == "#=GC") {
        iss >> token;
        if (token == "SS_cons") {
          iss >> conservedSecondaryStructure;
        }
        else if (token == "RF") {
          iss >> conservedSequence;
        }
      }
      else if (token == "//") {
        RfamEntry entry(name, conservedSequence, conservedSecondaryStructure, alignments);
        entries.push_back(entry);
        name = "";
        conservedSequence = "";
        conservedSecondaryStructure = "";
        alignments.clear();
      }
      else if (line.c_str()[0] != '#') {
        std::string readName = token;
        std::string readSequence;
        iss >> readSequence;
        alignment read = std::make_pair(readName, readSequence);
        alignments.push_back(read);
      }
    }
  }
  database = entries;
}

 bool RfamDatabase::IsDoubleStranded(const char ch) {
   return ch == '<' || ch == '>' || ch == '{' || ch == '}' || ch == '[' || ch == ']' || ch == '(' || ch == ')';
 }
