#ifndef __RfamParser__RfamEntry__
#define __RfamParser__RfamEntry__

#include <vector>
#include <string>

typedef std::pair<std::string, std::string> alignment;

class RfamEntry {
public:
  RfamEntry(std::string name, std::string conservedSequence, std::string conservedSecondaryStructure, std::vector<alignment> alignments);

  std::string Name() const;
  std::string ConservedSequence() const;
  std::string ConservedSecondaryStructure() const;
  std::vector<alignment> Alignments() const;
  alignment Alignment(std::string name) const;
  bool ContainsAlignment(std::string name) const;
private:
  std::string name;
  std::string conservedSequence;
  std::string conservedSecondaryStructure;
  std::vector<alignment> alignments;
};

#endif /* defined(__RfamParser__RfamEntry__) */
