//
//  RfamParser.h
//  RfamParser
//
//  Created by Kundan Chintamaneni on 7/19/15.
//  Copyright (c) 2015 Kundan Chintamaneni. All rights reserved.
//

#ifndef __RfamParser__RfamParser__
#define __RfamParser__RfamParser__

#include <vector>
#include "RfamEntry.h"

class RfamDatabase {
public:
  RfamDatabase(const std::string& filename);

  void Format();
  void Tabular();
private:
  bool IsDoubleStranded(const char ch);
   
  std::vector<RfamEntry> database;
};

#endif /* defined(__RfamParser__RfamParser__) */
