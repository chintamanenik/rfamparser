//
//  FastaUtility.cpp
//  RfamParser
//
//  Created by Kundan Chintamaneni on 7/19/15.
//  Copyright (c) 2015 Kundan Chintamaneni. All rights reserved.
//

#include "FastaUtility.h"

#include <iostream>
#include <fstream>

fasta_t FastaUtility::ParseFasta(const std::string& filename) {
  std::ifstream input(filename.c_str());
  std::vector<std::string> names;
  std::vector<std::string> sequences;
  std::string line;
  int counter = 0;
  std::cerr << "Parsing FASTA file: " << filename.c_str() << std::endl;
  while (std::getline(input, line)) {
    ++counter;
    if (counter == 1) {
      names.push_back(line.substr(1));
    }
    if (counter == 2) {
      counter = 0;
      sequences.push_back(line);
    }
  }
  return std::make_pair(names, sequences);
}