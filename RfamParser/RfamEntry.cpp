#include "RfamEntry.h"

#include <iostream>

RfamEntry::RfamEntry(std::string name, std::string conservedSequence, std::string conservedSecondaryStructure, std::vector<alignment> alignments) :
  name(name),
  conservedSequence(conservedSequence),
  conservedSecondaryStructure(conservedSecondaryStructure),
  alignments(alignments) {

  }


std::string RfamEntry::Name() const {
  return name;
}

std::string RfamEntry::ConservedSequence() const {
  return conservedSequence;
}

std::string RfamEntry::ConservedSecondaryStructure() const {
  return conservedSecondaryStructure;
}

std::vector<alignment> RfamEntry::Alignments() const {
  return alignments;
}

alignment RfamEntry::Alignment(std::string name) const {
  for (std::vector<alignment>::const_iterator it = alignments.begin(); it != alignments.end(); ++it) {
    if (it->first == name) {
      return *it;
    }
  }
  std::string noResult = "";
  return std::make_pair(noResult, noResult);
}

bool RfamEntry::ContainsAlignment(std::string name) const {
  for (std::vector<alignment>::const_iterator it = alignments.begin(); it != alignments.end(); ++it) {
    if (it->first == name) {
      return true;
    }
  }
  return false;
}
