//
//  FastaUtility.h
//  RfamParser
//
//  Created by Kundan Chintamaneni on 7/19/15.
//  Copyright (c) 2015 Kundan Chintamaneni. All rights reserved.
//

#ifndef __RfamParser__FastaUtility__
#define __RfamParser__FastaUtility__

#include <vector>


typedef std::pair<std::vector<std::string>, std::vector<std::string> > fasta_t;

class FastaUtility {
public:
  static fasta_t ParseFasta(const std::string& filename);
};

#endif /* defined(__RfamParser__FastaUtility__) */
